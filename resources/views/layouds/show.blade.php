<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

 <div>
    <h1>{{$game->name}}</h1>

    <h2>version:</h2>
    <p>{{$game->version}}</p>

    <h2>server size:</h2>
    <p>{{$game->serversize}}</p>

    <h2>server location:</h2>
    <p>{{$game->serverlocation}}</p>

    <h2>game type:</h2>
    <p>{{$game->gametype}}</p>

    <form action="/api/games/{{$game->id}}" method="post">
    @csrf
    @method('DELETE')
        <button>Delete</button>
    </form>
 </div>
    <a href="/api/games"><- back to all games</a>
</body>
</html>