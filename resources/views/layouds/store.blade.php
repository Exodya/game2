<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <div class="wrapper create-pizza">
        <h1>Create a New Game</h1>
        <form action="/api/games" method="POST">
        @csrf
            <label for="name">Name:</label>
            <input type="text" id="name" name="name">
            <label for="type">Version:</label>
            <select name="type" id="type">
                <option value="1.1">1.1</option>
                <option value="1.2">1.2</option>
                <option value="1.3">1.3</option>
                <option value="1.4">1.4</option>
                <option value=" 1.6">1.6</option>
            </select>
            <label for="serversize">serversize:</label>
            <input type="text" id="serversize" name="serversize">

            <label for="serverlocation">serverlocation:</label>
            <input type="text" id="serverlocation" name="serverlocation">

            <label for="gametype">gametype:</label>
            <input type="text" id="gametype" name="gametype">

            <input type="submit" value="Post">

          </form>
      </div>
    </body>
</html>
