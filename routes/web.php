<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () { return view('welcome'); });

Route::get('/api/games' , 'GameController@index');

Route::post('/api/games' , 'GameController@store');

Route::get('/api/games/{game}' , 'GameController@show');

Route::PATCH ('/api/games/{game}' , 'GameController@update');

Route::delete ('/api/games/{game}' , 'GameController@destroy');